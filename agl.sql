-- phpMyAdmin SQL Dump
-- version 5.0.4
-- https://www.phpmyadmin.net/
--
-- Hôte : localhost
-- Généré le : ven. 17 déc. 2021 à 14:18
-- Version du serveur :  10.4.17-MariaDB
-- Version de PHP : 8.0.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données : `agl`
--

-- --------------------------------------------------------

--
-- Structure de la table `materiel`
--

CREATE TABLE `materiel` (
  `id` int(11) NOT NULL,
  `type` int(11) NOT NULL,
  `label` varchar(30) NOT NULL,
  `status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `materiel`
--

INSERT INTO `materiel` (`id`, `type`, `label`, `status`) VALUES
(1, 1, 'Projecteur', 1),
(2, 2, 'Tableau blanc ', 0),
(3, 1, 'Projecteur', 1);

-- --------------------------------------------------------

--
-- Structure de la table `reservation`
--

CREATE TABLE `reservation` (
  `id` int(11) NOT NULL,
  `type` int(11) NOT NULL,
  `objet` varchar(30) NOT NULL,
  `date_reserv` datetime NOT NULL,
  `date_retour` datetime NOT NULL,
  `id_objet` int(11) NOT NULL,
  `status` int(11) NOT NULL,
  `description` varchar(100) DEFAULT NULL,
  `reservant` varchar(30) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `reservation`
--

INSERT INTO `reservation` (`id`, `type`, `objet`, `date_reserv`, `date_retour`, `id_objet`, `status`, `description`, `reservant`) VALUES
(6, 1, 'IRAN-2 ( 1 ) ', '2021-12-16 09:00:00', '2021-12-16 12:00:00', 1, 0, 'Cours d\'ART Plastique , IRAN 2 , ( GL , SI , IM )', ''),
(7, 0, 'Projecteur ( 3 ) ', '2021-12-03 19:16:00', '2021-12-04 19:16:00', 3, 0, 'Cours d\'astique ( GL , SI , IM )', '0'),
(8, 0, 'Projecteur ( 3 ) ', '2021-12-03 19:16:00', '2021-12-04 19:16:00', 3, 1, 'Cours d\'qssqcstique ( GL , SI , IM )', '0'),
(9, 0, 'Projecteur ( 1 ) ', '2021-12-03 19:16:00', '2021-12-04 19:16:00', 1, 0, 'Cours de poésie ( GL , SI , IM )', '0'),
(10, 1, 'Salle MOOC ( 2 ) ', '2021-12-15 14:30:00', '2021-12-15 17:00:00', 2, 1, 'Cours de BDA , Padtice , ( GL , SI , IM )', NULL),
(11, 1, 'IRAN-2 ( 1 ) ', '2021-12-17 12:00:00', '2021-12-17 14:00:00', 1, 0, NULL, NULL),
(12, 0, 'Projecteur ( 1 ) ', '2021-12-17 14:14:00', '2021-12-17 18:14:00', 1, 1, NULL, NULL),
(13, 1, 'IRAN-2 ( 1 ) ', '2021-12-18 16:16:00', '2021-12-18 18:16:00', 1, 1, 'dcvdsv<vd', NULL);

-- --------------------------------------------------------

--
-- Structure de la table `salle`
--

CREATE TABLE `salle` (
  `id` int(11) NOT NULL,
  `label` varchar(30) NOT NULL,
  `Capacité` int(11) NOT NULL,
  `Localisation` varchar(100) NOT NULL,
  `status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `salle`
--

INSERT INTO `salle` (`id`, `label`, `Capacité`, `Localisation`, `status`) VALUES
(1, 'IRAN-2', 1000, 'Deuxième étage du bâtiment de la FSA ', 1),
(2, 'Salle MOOC', 1000, 'En haut du batiment mooc', 1);

-- --------------------------------------------------------

--
-- Structure de la table `user`
--

CREATE TABLE `user` (
  `id` varchar(30) NOT NULL,
  `pass` varchar(30) NOT NULL,
  `nom` varchar(30) NOT NULL,
  `prenom` varchar(30) NOT NULL,
  `type` int(11) NOT NULL,
  `tel` varchar(30) DEFAULT NULL,
  `email` varchar(30) DEFAULT NULL,
  `profil` varchar(100) DEFAULT NULL,
  `formation` varchar(30) DEFAULT NULL,
  `heures` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `user`
--

INSERT INTO `user` (`id`, `pass`, `nom`, `prenom`, `type`, `tel`, `email`, `profil`, `formation`, `heures`) VALUES
('13344091', '1234', 'Jean', 'Gérônime', 1, NULL, NULL, NULL, NULL, NULL),
('13366220', '1234', 'ASSOGBA ', 'Ninel Orcy Fifa', 4, '+229 65766464', 'ninelorcy19@gmail.com', '../assets/profil/p4.jpg', NULL, 40),
('13367020', '1234', 'DAVAKAN', 'Jeffrey', 1, '+22960533466', 'jeffrey.dvk.fr@gmail.com', '../assets/profil/p5.jpg', 'Génie Logiciel 3', NULL);

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `materiel`
--
ALTER TABLE `materiel`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `reservation`
--
ALTER TABLE `reservation`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `salle`
--
ALTER TABLE `salle`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT pour les tables déchargées
--

--
-- AUTO_INCREMENT pour la table `materiel`
--
ALTER TABLE `materiel`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT pour la table `reservation`
--
ALTER TABLE `reservation`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT pour la table `salle`
--
ALTER TABLE `salle`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
