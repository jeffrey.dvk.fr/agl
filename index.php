<?php 
if( session_status() != PHP_SESSION_ACTIVE){ session_start();}
if(!empty($_SESSION['user'])){
    header('Location: pages/dashboard.php');
    exit();
} ?>
<!DOCTYPE html>
<html lang="fr">
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Connexion</title>
        <link rel="stylesheet" type="text/css" href="css/bootstrap/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="md/css/clean-switch.css">
        <link rel="stylesheet" type="text/css" href="icon/fontawesome/css/all.css">
        <link rel="stylesheet" type="text/css" href="md/css/bttn.css/dist/bttn.min.css"/>
        <link rel="stylesheet" type="text/css" href="md/datatables/datatables.min.css">
        <link rel="stylesheet" type="text/css" href="md/js/timetable/dist/styles/timetablejs.css">
        <link rel="stylesheet" type="text/css" href="md/datatables/DataTables-1.10.16/css/dataTables.bootstrap4.min.css">
        <link rel="stylesheet" type="text/css" href="md/datatables/Select-1.2.4/css/select.bootstrap4.min.css">
        <link rel="stylesheet" type="text/css" href="css/main.css">
        <link rel="stylesheet" type="text/css" href="css/login.css">
    </head>
    <body>
        <div class="container">
            <form action="php/connection.php" method="POST" class="" autocomplete="off">
                <h3>Connexion</h3>
                <div class="inputs">
                    <label for="user"><i class="fas fa-user"></i></label>
                    <input id="user" type="text" name="id" placeholder="Identifiant">
                </div>
                <div class="inputs">
                    <label for="password"><i class="fas fa-lock"></i></label>
                    <input id="password" type="password" name="pass" placeholder="Mot de passe">
                </div>    
                <div>
                    <label class="cl-switch">
                        <input type="checkbox" checked>
                        <span class="switcher"></span>
                        <span class="label">Se souvenir de moi</span>
                    </label>           
                </div>
                <div>
                    <button class="">Se connecter    </button>   
                </div>
                <h3></h3>
                <div class="forgot">
                    <a href="#">Mot de passe oublié?</a></p>
                </div>
            </form>
        </div>  
    </body>
</html>