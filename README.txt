Processus pour installer l'application

1- Importer la base de donnée ( fichier agl.sql )
2- Démarrer votre serveur en vous positionnant dans le répertoire de l'application
3- Connecter vous en utilisant les identifiants de connections suivants :
	-> Rôle Étudiant
		- ID : 13367020 
		- pass : 1234
	-> Rôle administrateur
		- ID : 13366220
