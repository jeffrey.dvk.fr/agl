<?php 
if( session_status() != PHP_SESSION_ACTIVE){ session_start();}
if(empty($_SESSION['user'])){
    header('Location: ../index.php?erreur=connection');
    exit();
}
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
    	<meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Dashboard</title>
        <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css">
    	<link rel="stylesheet" type="text/css" href="../icon/fontawesome/css/all.css">
        <link rel="stylesheet" type="text/css" href="../md/css/bttn.css/dist/bttn.min.css"/>
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/sweetalert2@8/dist/sweetalert2.min.css">
        <link rel="stylesheet" type="text/css" href="../md/datatables/datatables.min.css">
        <link rel="stylesheet" type="text/css" href="../md/js/stepper/bs-stepper.min.css">
        <link rel="stylesheet" type="text/css" href="../md/css/clean-switch.css">
        <link rel="stylesheet" type="text/css" href="../md/js/virtual-select/virtual-select.min.css">
        <link rel="stylesheet" type="text/css" href="../md/js/timetable/dist/styles/timetablejs.css">
        <link rel="stylesheet" type="text/css" href="../md/datatables/DataTables-1.10.16/css/dataTables.bootstrap4.min.css">
        <link rel="stylesheet" type="text/css" href="../md/datatables/Select-1.2.4/css/select.bootstrap4.min.css">
        <link rel="stylesheet" type="text/css" href="../css/main.css">
        <link rel="stylesheet" type="text/css" href="../css/dashboard.css">
        <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.2/dist/umd/popper.min.js"></script>
        <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.min.js"></script>
        <script type="text/javascript" src="../md/js/jquery-3.6.0.min.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/sweetalert2@8"></script>
        <script>
            $(document.main).css("opacity","0")
            document.onreadystatechange = function(e){
                if( document.readyState  === 'complete'){
                    setTimeout(() => {
                        $(".loader").css("opacity","0")
                        setTimeout(() => {
                            $(document.main).css("opacity","1")
                            $(".loader").remove()
                        }, 1000);
                    }, 1000);
                } 
            }
            function loader(){
                $(".content").append($(
                    `<div class="loader-2">
                        <div class="center">
                            <div style="--i: 1" class="wave"></div>
                            <div style="--i: 2" class="wave"></div>
                            <div style="--i: 3" class="wave"></div>
                            <div style="--i: 4" class="wave"></div>
                            <div style="--i: 5" class="wave"></div>
                            <div style="--i: 6" class="wave"></div>
                            <div style="--i: 7" class="wave"></div>
                            <div style="--i: 8" class="wave"></div>
                            <div style="--i: 9" class="wave"></div>
                            <div style="--i: 10" class="wave"></div>
                        </div>
                    </div>`
                ))
                $(".loader-2").css("opacity","1")
            }
            function load(url="planning.php"){
                $(".content").css("opacity","0")
                setTimeout(()=>{
                    $(".content").html("")
                    loader()
                    $(".content").css("opacity","1")
                    setTimeout(()=>{
                        $.get(url,{},function(data){
                            $(".content").css("opacity","0")
                            setTimeout(()=>{
                                $(".loader-2").remove()
                                $(".content").html(data)
                                setTimeout(()=>{
                                    $(".content").css("opacity","1")
                                },500)
                            },500)
                        })
                    },500)
                },500)
            } 
            load() 
    	</script>
    </head>
    <body>
        <div class="loader">
            <span style="--i: 1">C</span>
            <span style="--i: 2">H</span>
            <span style="--i: 3">A</span>
            <span style="--i: 4">R</span>
            <span style="--i: 5">G</span>
            <span style="--i: 6">E</span>
            <span style="--i: 7">M</span>
            <span style="--i: 8">E</span>
            <span style="--i: 9">N</span>
            <span style="--i: 10">T</span>
            <span style="--i: 11">.</span>
            <span style="--i: 12">.</span>
            <span style="--i: 13">.</span>
        </div>
        <main>
            <section class="top-bar flex">
                <button class="openNav active"><i class="fas fa-caret-square-right"></i></button>
                <div class="flex pg-link flex" data-page="Profil">
                    <img src="<?= $_SESSION["profil"] ?>">
                    <p><?= $_SESSION["nom"] ?></p>
                </div>
            </section>
            <section class="agl-container flex">
                <div class="nav-bar">
                    <ul>
                        <?php if($_SESSION['type']!=1): ?>
                            <li class="pg-link" data-page="Accueil"><i class="fas fa-hammer"></i><span>Dashboard</span></li>
                            <li class="pg-link" data-page="Reservation"><i class="fas fa-wrench"></i><span>Réservation</span></li>
                        <?php endif ?>
                        <li class="pg-link" data-page="Planning"><i class="fas fa-calendar-alt"></i><span>Planning</span></li>
                        <li class="pg-link" data-page="Profil"><i class="fas fa-user-cog"></i><span>Profil</span></li>
                        <li><i class="fas fa-sign-out-alt logout"></i><span>Déconnexion</span></li>
                    </ul>
                </div>
                <div class="content"></div>
            </section>
        </main>
        
    	
    	
    	
        <script type="text/javascript" src="../md/js/stepper/bs-stepper.min.js"></script>
        <script type="text/javascript" src="../md/datatables/datatables.min.js"></script>
        <script type="text/javascript" src="../md/datatables/Select-1.2.4/js/dataTables.select.min.js"></script>
        <script type="text/javascript" src="../md/js/modules-datatables.js"></script>
        <script type="text/javascript" src="../md/js/virtual-select/virtual-select.min.js"></script>
        <script type="text/javascript" src="../md/js/sweetalert.min.js"></script>
        <script type="text/javascript" src="../md/js/timetable/dist/scripts/timetable.js"></script>
        <script type="text/javascript" src="../js/action.js"></script>
        <script>
            $(".pg-link").on("click",function(e){
                page = $(this).data('page').toLowerCase()
                load(page+".php")
            })
            $(".logout").on("click",()=>{
                $.ajax({
                    url:"../php/deconnexion.php",
                    method:"POST",
                    data:{
                        action: "",
                    },
                    dataType:"json",
                    success:function(data)
                    {
                        console.log("Yo")
                        window.location.replace("../index.php");
                    }
                })
            })
            
        </script>

        
    	
    </body>
</html>
