<?php session_start() ?>
<section class="accueil-section">
        <?php 
        if($_SESSION['type']==4){
            echo '<button class="bttn-fill bttn-md bttn-primary" data-bs-toggle="modal" data-bs-target="#signuser">Créer un utilisateur</button>';
        }if($_SESSION['type']>1){
            echo '<button data-page="Reservation" class="pg-link bttn-fill bttn-md bttn-success">Faire une réservation</button>';
        }if($_SESSION['type']>2){
            echo '<button class="bttn-fill bttn-md bttn-danger">Éditer le récapitulatif horaire </button>';
        }
        if($_SESSION['type']==4){
            echo '<button class="bttn-fill bttn-md bttn-primary" data-bs-toggle="modal" data-bs-target="#signuser">Insérer un Matériel/Salle</button>';
        }
        ?>    
</section>
<?php if($_SESSION['type']==4): ?>
    <div class="modal fade" id="signuser" data-bs-keyboard="false" tabindex="-1" aria-labelledby="chambre1Label" aria-hidden="true">
        <div class="modal-dialog modal-md">
            <div class="modal-content">
                <div class="modal-body">
                    <h3>Créer un utilisateur</h3>
                    <form>
                        <div id="type-select"></div>
                        <div class="others-div">
                            <label for="">Nom :</label>
                            <input name="nom" type="text" placeholder="Ex : SheepQueen">
                        </div>
                        <div class="others-div">
                            <label for="">Prénom :</label>
                            <input name="prenom" type="text" placeholder="Ex : Nini">
                        </div>
                        <div class="others-div">
                            <label for="">Identifiant :</label>
                            <input name="id" type="text" placeholder="Ex : Matricule ">
                        </div>
                        <div>
                            <button>Créer</button>
                        </div>
                    </form>
                    
                </div>
            </div>
        </div>
</div>
<?php endif ?>
<script>
    $(".pg-link").on("click",function(e){
        load($(this).data('page').toLowerCase() +".php")
    })
    VirtualSelect.init({
        ele: '#type-select',
        options: [
            { label: 'Administrateur',value: '4',},
            { label: 'Professeur responsable',value: '3',},
            { label: 'Professeur',value: '2',},
            { label: 'Etudiant',value: '1',}
        ],
        placeholder: 'Type de compte',
    });
    var typeSellect =  document.querySelector('#type-select');
    $("#signuser form").on("submit",function(e){
        e.preventDefault()
        let datas = new FormData(e.target)
        if(datas.get("nom") == '' || datas.get("prenom") == '' || datas.get("id") == '' || typeSellect.value == ""){
            Swal.fire({
                type: 'error',
                title: 'Informations incomplètes',
                text: 'Veuiller remplir tout les champs',
            })
        }else{
            $.ajax({
                url: "../php/action.php",
                method: "POST",
                data: {
                    action : "registerUser",
                    value : {
                        nom : datas.get("nom"),
                        prenom : datas.get("prenom"),
                        id : datas.get("id"),
                        type : typeSellect.value
                    }
                },
                dataType: "JSON",
                success:function(data){
                    if( "error" in data ){
                        Swal.fire({
                            type: 'error',
                            title: 'Un problème est survenu',
                            text: data["error"],
                        })
                    }else{
                        Swal.fire({
                            type: 'success',
                            title: 'Effectué',
                            html: "Utilisateur créé avec succès<br><br>Informations de connexion<br><br>Identifiant   :   <b>"+datas.get("id")+"</b><br>Mot de passe   :   <b>1234</b>",
                        })
                        $("#signuser").modal('hide');
                        document.querySelector('#type-select').reset()
                        $("#signuse form").reset()
                    }
                }
            })
        }
    })
</script>