<?php
session_start();
$column = [ 'N°', 'Matériel/Salle réservée', 'Date réservation',"Date de retour","Status","Action"];
?>
<section class="reservation">
    <?php if($_SESSION['type'] !=1 ): ?>
    <div class="make-reservation flex" style="">
        <button id="reserv" class="bttn-fill bttn-md bttn-primary" data-bs-toggle="modal" data-bs-target="#reservation">Faire une réservation<i class="fas fa-edit ms-2"></i></button>
    </div>
    <?php endif ?>
    <table id="reservationsTable" class="table table-striped table-bordered" cellspacing="0" style="width:100%;">
        <thead>
            <tr>
                <?php foreach($column as $col): ?>
                    <th class="border-top th-style"><?= $col ?></th>
                <?php endforeach ?>
            </tr>
        </thead>
    </table>
    <div class="modal fade" id="reservation" data-bs-keyboard="false" tabindex="-1" aria-labelledby="chambre1Label" aria-hidden="true">
        <div class="modal-dialog modal-md">
            <div class="modal-content">
                <div class="modal-body">
                    <section id="stepper" class="bs-stepper horizontal content">
                        <section class="bs-stepper-header reservation-options" role="tablist">
                            <button class="btn-reset step active" data-target="#step1" >
                                <span class="step-trigger" role="tab">
                                    Réservation de matérielle
                                </span>
                            </button>
                            <button class="btn-reset step" data-target="#step2" >
                                <span class="step-trigger" role="tab">
                                    Réservation de salle
                                </span>
                            </button>     
                        </section>
                        <section class="steps bs-stepper-content">
                            <section id="step1" role="tabpanel" class="bs-stepper-pane active" >
                                <form>
                                    <div id="type-select"></div>
                                    <div id="materials-select"></div>
                                    <input type="text" class="dates" id="" placeholder="Date et Heure de réservation">
                                    <input type="text" class="dates" id="" placeholder="Date et Heure de retour">
                                    <textarea class="comment" placeholder="Entrer le cours à effectué et les promotions concernés"></textarea>
                                    <div>
                                        <button >Annuler</button>
                                        <button type="submit">Réserver</button>
                                    </div>
                                </form>
                            </section>
                            <section id="step2" role="tabpanel" class="bs-stepper-pane " >
                                <form>
                                    <div id="salle-select"></div>
                                    <input type="text" class="dates" id="" placeholder="Date et Heure de réservation">
                                    <input type="text" class="dates" id="" placeholder="Date et Heure de retour">
                                    <textarea class="comment" placeholder="Entrer le cours à effectué et les promotions concernés"></textarea>
                                    <div>
                                        <button >Annuler</button>
                                        <button type="submit">Réserver</button>
                                    </div>
                                </form>
                            </section>
                        </section>
                    </section>
                </div>
            </div>
        </div>
    </div>
<script>
    $("#reserv").on("click",function(e){
        $.ajax({
            url: "../php/action.php",
            method: "POST",
            data: {
                action : "getSalles",
            },
            dataType: "JSON",
            success:function(data){
                if( !("error" in data)){
                    let disable = []
                    let values = []
                    let sallesSelect = document.querySelector('#salle-select');
                    sallesSelect.reset();
                    data.forEach((salle)=>{
                        values.push({label: salle.label ,value: salle.id})
                        if(salle.status == 1){
                            disable.push(salle.id)
                        }
                    })
                    sallesSelect.setDisabledOptions(disable);
                    sallesSelect.setOptions(values);
                }
            }
        })
    })
    var sallesSelect = document.querySelector('#salle-select');
    var stepper = new Stepper($('.bs-stepper')[0],{
        linear: false,
        animation: false
    })
    if(typeof reservationsTable !== undefined){
        var reservationsTable = $('#reservationsTable').DataTable({
            "processing":true,
            "serverSide":true,
            "order":[],
            "ajax":{
                url:"../php/data.php",
                type:"POST",
            },
            "columns":[
                { data : 'id'},
                { data : 'objet'},
                { data : 'date_reserv'},
                { data : 'date_retour'},
                { data : 'status'},
                { data : 'action'},
            ],
            "columnDefs":[
                {
                    "targets":[5],
                    "orderable":false,
                },
            ],
            "pageLength": 10
        });
    }else{
        reservationsTable.ajax.reload();
    }
    var materialsSelect = document.querySelector('#materials-select'); 
    VirtualSelect.init({
        ele: '#type-select',
        options: [{ label: 'Projecteur',value: '1',},{ label: 'Tableau blanc',value: '2',}],
        search: true ,
        placeholder: 'Type',
    });
    VirtualSelect.init({
        ele: '#materials-select',
        options: [],
        multiple: true ,
        search: true ,
        placeholder: 'Matériel',
        disabledOptions: [2],
    });
    VirtualSelect.init({
        ele: '#salle-select',
        options: [],
        search: true ,
        placeholder: 'Salles',
        disabledOptions: [2],
    });
    $(".dates").on("focus",function(e){
        this.type = 'datetime-local';
        this.focus();
    })
    $(".dates:first-of-type").on("change",function(e){
        $(".dates:last-of-type").attr("min", this.value);
    })
    $(".steps button:first-of-type").on("click",function(e){
        e.preventDefault()
        $("#reservation").modal('hide');
    })
    $(".steps > section:first-of-type form").on("submit",function(e){
        e.preventDefault()
        let dates = $(this).children(".dates")
        let datas = {
            type: $("#type-select").val(),
            materials : $("#materials-select").val(),
            dateRes : dates[0].value,
            dateRet : dates[1].value,
            desc : $(this).children(".comment").val()
        }
        if(datas.type == '' || datas.materials == [] || datas.dateRes == "" || datas.dateRet == ""){
            Swal.fire({
                type: 'error',
                title: 'Informations incomplètes',
                text: 'Veuiller remplir tout les champs',
            })
        }else{
            $.ajax({
                url: "../php/action.php",
                method: "POST",
                data: {
                    action : "makeResMat",
                    value : datas
                },
                dataType: "JSON",
                success:function(data){
                    if( "error" in data ){
                        Swal.fire({
                            type: 'error',
                            title: 'Un problème est survenu',
                            text: data["error"],
                        })
                    }else{
                        Swal.fire({
                            type: 'success',
                            title: 'Effectué',
                            text: "Réservation effectuée avec succès",
                        })
                        reservationsTable.ajax.reload();
                        $("#reservation").modal('hide');
                        document.querySelector('#type-select').reset()
                        document.querySelector('#materials-select').reset()
                        $(".steps > section:first-of-type form").reset()
                    }
                }
            })
        }
    })
    $(".steps > section:last-of-type form").on("submit",function(e){
        e.preventDefault()
        let dates = $(this).children(".dates")
        let datas = {
            salle : $("#salle-select").val(),
            dateRes : dates[0].value,
            dateRet : dates[1].value,
            desc : $(this).children(".comment").val()
        }
        if( datas.salle == [] || datas.dateRes == "" || datas.dateRet == ""){
            Swal.fire({
                type: 'error',
                title: 'Informations incomplètes',
                text: 'Veuiller remplir tout les champs',
            })
        }else{
            $.ajax({
                url: "../php/action.php",
                method: "POST",
                data: {
                    action : "makeResSalle",
                    value : datas
                },
                dataType: "JSON",
                success:function(data){
                    if( "error" in data ){
                        Swal.fire({
                            type: 'error',
                            title: 'Un problème est survenu',
                            text: data["error"],
                        })
                    }else{
                        Swal.fire({
                            type: 'success',
                            title: 'Effectué',
                            text: "Réservation effectuée avec succès",
                        })
                        reservationsTable.ajax.reload();
                        $("#reservation").modal('hide');
                        document.querySelector('#salle-select').reset()
                        $(".steps > section:last-of-type form").reset()

                    }
                }
            })
        }
    })
    $("#type-select").on("change",function(e){
        let val = this.value ;
        $.ajax({
            url: "../php/action.php",
            method: "POST",
            data: {
                action : "getMaterial",
                value : val
            },
            dataType: "JSON",
            success:function(data){
                if( !("error" in data)){
                    let disable = []
                    let values = []
                    materialsSelect.reset();
                    data.forEach((material)=>{
                        values.push({label: material.label+" ( <i>"+material.id+"</i> ) " ,value: material.id})
                        if(material.status == 1){
                            disable.push(material.id)
                        }
                    })
                    materialsSelect.setDisabledOptions(disable);
                    materialsSelect.setOptions(values);
                }
            }
        })
    })
    $("body").on('click', '.dropdown-menu li a.cancel', function(e){
        e.preventDefault();
        let id= $(this).attr("id")
        $.ajax({
            url: "../php/action.php",
            method: "POST",
            data: {
                action : "cancelRes",
                id: id
            },
            dataType: "JSON",
            success:function(data){
                if( "error" in data ){
                    Swal.fire({
                        type: 'error',
                        title: 'Un problème est survenu',
                        text: data["error"],
                    })
                }else{
                    Swal.fire({
                        type: 'success',
                        title: 'Effectué',
                        text: "Réservation effectuée avec succès",
                    })
                    reservationsTable.ajax.reload();
                }
            }
        })
    })
</script>

</section>