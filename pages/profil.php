<?php session_start() ?>
<section class="profil-section">
    <div class="flex user-detail">
        <div>
            <img src="<?= $_SESSION["profil"] ?>" alt="">
            <button><i class="fas fa-pen"></i></button>
        </div>
        <div class="user-info">
            <p><?= $_SESSION['nom'] ?></p>
            <p><?= $_SESSION['rank'] ?></p>
        </div>
    </div>
    <div class="user-rank">
        <?php if($_SESSION['type']!=1){
            echo "<p>Récapitulatif horaire : <span>".$_SESSION['heure']."h</span></p>";
        }else{
            echo "<p>Formation : <span>".$_SESSION['formation']."</span></p>";
        }
        ?>
    </div>
    <div class="password-change">
        <h3>Modification du mot de passe</h3>
        <form>
            <input type="password" name="" id="pass" placeholder="Ancien mot de passe">
            <input type="password" name="" id="new_pass" placeholder="Nouveau mot de passe">
            <div class="text-end">
                <button class="bttn-fill bttn-md bttn-primary">Modifier</button>
            </div>
        </form>
    </div>
</section>
