<?php
session_start();
require_once '_dbfunction.php';

if(!empty($_POST['id']) && !empty($_POST['pass'])){
    $user = searchUser("user",$_POST['id'],$_POST['pass']);
    if(count($user) > 0){
        $user = $user[0];
        $_SESSION["user"] = true;
        $_SESSION["id"] = $user["id"];
        $_SESSION["type"] = $user["type"];
        $_SESSION["profil"] = $user["profil"];
        $_SESSION["nom"] = $user["nom"]." ".$user["prenom"];
        $_SESSION["tel"] = $user["id"];
        $_SESSION["rank"] = getRank($user["type"]);
        if($user['type']!=1){
            $_SESSION["heure"] = $user["heures"];
        }else{
            $_SESSION["formation"] = $user["formation"];
        }
        header('Location: ../pages/dashboard.php');
        exit();
    }
}else{
    header('Location: ../pages/dashboard.php?erreur=true');
}
function getRank($type){
    switch($type){
        case 1:
            return "Etudiant";
            break;
        case 2:
            return "Professeur";
            break;
        case 3:
            return "Professeur Responsable";
            break;
        case 4:
            return "Administrateur";
            break;
    }
}
?>