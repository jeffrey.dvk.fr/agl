<?php
$pdo = new PDO('mysql:host=localhost;dbname=agl', 'root', '',[PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION]);
$pdo->exec('SET NAMES utf8');
$pdo->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);
/*
Cette fonction permet l'insertion d'une donnée dans une table 
Cette fonction prend deux paramètre
- $table , la table dans laquelle l'insertion sera faite
- $data , un tableau associatif dont les clé sont les colonnes de la tables à renseigner 
et les valeurs sont les valeurs qui leurs seront associés
Ex : insert( 'user' , [
    'nom' => 'MAHOUSSOU',
    'prenom' => 'Robert',
    'mdp' => hash(1234)
])
NB: Toutes colonnes non renseignées doivent posséder une valeur par défaut dans la table
*/
function insert($table,$data) {
    global $pdo;
    $values = array_values($data);
    $columns = implode(' , ' , array_keys($data) );
    $mark = [];
    for( $i = 0 ; $i< count($values) ; $i++){
        $mark[] = '?';
    }
    $marker = implode(' , ' , $mark );
    $query = "INSERT INTO $table ( $columns ) VALUES ( $marker );";
    $statement = $pdo->prepare($query);
    $statement->execute(array_values($data));
}
/*
Cette fonction permet la modification d'un enregistrement dans une table 
Cette fonction prend quatre paramètres
- $table , la table possédant l'enregistrement
- $data , un tableau associatif dont les clé sont les colonnes de la tables à renseigner 
et les valeurs sont les valeurs qui leurs seront associés
- $col , la colonne de l'attribut utiliser pour rechercher l'enregistrement
- $val , la valeur de la colonne pour l'enregistrement
Ex : update( 'user' , [
    'prenom' => 'Roberto',
    'mdp' => hash(12345)
],'id',18)
NB: Toutes colonnes non renseignées doivent posséder une valeur par défaut dans la table
*/
function update($table,$data,$col,$val){
    global $pdo;
    $values = array_values($data);
    $columns = implode(' , ' , array_keys($data) );
    $replace = '';
    foreach($data as $key => $value){
        $replace .= $key.' = ? , ';
    }
    $replace = chop($replace , ', ');
    $query = "UPDATE $table set $replace WHERE $col = $val;";
    $statement = $pdo->prepare($query);
    $statement->execute(array_values($data));
}
/*
Cette fonction permet la suppression d'un enregistrement dans une table 
Cette fonction prend trois paramètres
- $table , la table possédant l'enregistrement
- $col , la colonne de l'attribut utiliser pour rechercher l'enregistrement
- $val , la valeur de la colonne pour l'enregistrement
Ex : update( 'user','id', 3)
NB: Toutes colonnes non renseignées doivent posséder une valeur par défaut dans la table
*/
function delete($table,$col,$val){
    global $pdo;
    $query = "DELETE FROM $table WHERE $col = ? ";
    $statement = $pdo->prepare($query);
    $statement->execute([$val]);
}

function getOptions($table,$columns,$active,$order){
    global $pdo;
    $query = "SELECT * FROM $table WHERE $active = 'Actif' ORDER BY $order ASC ;";
    $result = execute($query);
    $output = '';
    $id = array_shift($columns);
    foreach($result as $row)
    {
        $output .= '<option value="'.$row[$id].'">';
        foreach($columns as $col){
            $output .= $row[$col]." ";
        }
        $output = chop($output , " ");
        $output .='</option>';
    }
    return $output;
}
function getData($table){
    global $pdo;
    $query = "SELECT * FROM $table";
    return execute($query);
}
function getDataWhereLike($table,$params,$search,$more = ''){
    global $pdo;
    $query = "SELECT * FROM $table WHERE ";
    foreach($params as $p){
        $query .= "$p LIKE '%$search%' OR ";
    }
    $query = chop($query , "OR ") . $more;
    return execute($query);
}
function searchUser($table,$id,$pass){
    global $pdo;
    $query = "SELECT * FROM $table WHERE id = $id AND pass = $pass";
    return execute($query);
}
function getDataWhere($table,$param,$value){
    global $pdo;
    $query = "SELECT * FROM $table WHERE $param = $value";
    return execute($query);
}
function getDataWhereAnd($table,$where){
    global $pdo;
    $query = "SELECT * FROM $table WHERE ";
    foreach(array_keys($where) as $w){
        $query .= "$w = ? AND ";
    }
    $query = chop($query , "AND ");
    return execute($query,array_values($where));
}

function execute($query,$params = []){
    $statement = set($query,$params);
    return $statement->fetchAll();
}
function set($query,$params = []){
    global $pdo;
    $statement = $pdo->prepare($query);
    if( count($params) >= 0 )
        $statement->execute($params);
    else
        $statement->execute();
    return $statement;
}
function addlog($CodeEvent, $ParamEvent, $PseudoUtilisateur){
    global $pdo;
    $varLogDate = gmdate("Y-m-d");
    $varLogHeure = gmdate("H:i:s"); //strftime("%H:%M:%S");
    $varCodeEvent = $CodeEvent;
    $varParamEvent = $ParamEvent;
    $varLogPseudo = addslashes($PseudoUtilisateur);
    $varLogIP = getenv("REMOTE_ADDR");
    $query_Clients = "INSERT INTO addlog_table(`CodeEvenement`,`ParametresEvenement`,`DateEvenement`,`HeureEvenement`,`PseudoUtilisateur`,`AdresseIP`) VALUES ('". $CodeEvent . "','" . $varParamEvent . "','" . $varLogDate . "','" . $varLogHeure ."','". $varLogPseudo . "','" . $varLogIP . "')";
    $statement = $pdo->prepare($query_Clients);
    $statement->execute();
}

