<?php
session_start();   
include('../php/_dbfunction.php');

$data = [];
if( isset($_POST['action']) ){
    switch($_POST['action']){
        case "getMaterial":
            $val = $_POST['value'];
            $data = getDataWhere("materiel","type", $val);
            break;
        case "getSalles":
            $data = getData("salle");
            break;
        case "makeResMat":
            $datas = $_POST['value'];
            if(strtotime($datas["dateRes"]) >= $datas["dateRet"]){
                $err = "La date de réservation ne peut être inférieure ou égale à la date de retour";
            }else{
                if(is_array($datas["materials"])){
                    foreach($datas["materials"] as $material){
                        $err = checkError(verifyAndInsert($material,"materiel",0));
                    }
                }else{
                    $err = checkError(verifyAndInsert($datas["materials"],"materiel",0));
                }
            }
            break;
        case "makeResSalle":
            $datas = $_POST['value'];
            if(strtotime($datas["dateRes"]) >= $datas["dateRet"]){
                $err = "La date de réservation ne peut être inférieure ou égale à la date de retour";
            }else{
                $err = checkError(verifyAndInsert($datas["salle"],"salle",1));
            }
            break;
        case "cancelRes":
            $obj = getDataWhere("reservation","id", $_POST["id"]);
            if( count($obj)>0){
                if($obj[0]["type"]==0){
                    update( "materiel" , [ 'status' => '0' ], 'id', $obj[0]["id_objet"]);
                }else{
                    update( "salle" , [ 'status' => '0' ], 'id', $obj[0]["id_objet"]);
                }
                update( "reservation" , [ 'status' => '0' ],'id',$_POST["id"]);
            }else{
                $err = "Réservation non trouvée";
            }
            break;
        case "registerUser":
            $datas = $_POST['value'];
            if(count(getDataWhere("user","id", $datas["id"]))>0){
                $err = "Un utilisateur avec ce identifiant existe déjà";
            }else{
                insert( 'user' , [
                    'id' => $datas["id"],
                    'pass' => "1234",
                    'nom' => $datas["nom"],
                    'prenom' => $datas["prenom"],
                    'type' => $datas["type"],
                    
                ]);
            }
            break;
        case "getPlanning":
            $date = $_POST['date'];
            $date =  new DateTime(date("Y-m-d", strtotime('monday this week', strtotime($date))));
            $datas = "<thead>
                        <tr>
                            <th>Semaine du <br> ".$date->format("d-m-Y")."</th>
                            <th>Lundi</th>
                            <th>Mardi</th>
                            <th>Mercredi</th>
                            <th>Jeudi</th>
                            <th>Vendredi</th>
                            <th>Samedi</th>
                        </tr>
                    </thead>
                    <tbody>";
            $reservations = getDataWhere("reservation","type", 1);
            for($i=7; $i<19; $i++){
                $datas .= '<tr>
                            <td class="hours"> '.$i."h - ".($i+1)."h </td>";
                $all = getBegin($i,$date,$reservations);
                $s = 5;
                $c = clone $date;
                while($s>=0){
                    $res = getThisDayRes($c,$all);
                    $c->modify('+1 day');
                    if(count($res)>0){
                        $datas .= '<td class="active">';
                        foreach($res as $r){
                            $datas .= "<div><span>".$r["description"]."</span><br>Prof.".$r["reservant"]."</div>";
                        }
                        $datas .= "</td>";
                    }else{
                        $datas .= '<td class="inactive"></td>';
                    }
                    
                    $s--;
                }
            }
            $data = $datas;
        default:
            break;
    }
}else{
    $err = "Aucune action à effectuée";
}
if(isset($err) && $err != ""){
    $data = ["error"=> $err];
}
echo json_encode($data);
function verifyAndInsert($objet,$table,$type){
    global $datas;
    $obj = getDataWhere($table,"id", $objet);
    if(count($obj)>0){
        if($obj[0]["status"] == 1){
            return -2;
        }
        update( $table , [ 'status' => '1' ],'id',$objet);
        insert( 'reservation' , [
            'type' => $type,
            'objet' => $obj[0]["label"]." ( ".$obj[0]["id"]." ) ",
            'date_reserv' => $datas["dateRes"],
            'date_retour' => $datas["dateRet"],
            'id_objet' => $objet,
            "status" => 1,
            "description" => $datas["desc"]
            
        ]);
        return 1;
    }else{
        return -1;
    }
}
function checkError($i){
    switch($i){
        case -1:
            return "Matériel/Salle non trouvé";
            break;
        case -1:
            return "Matériel/Salle déjà réservé";
            break;
        default:
            return "";
            break;
    }
}
function getBegin($i,$date,$reservations){
    $date = new DateTime("2021-01-10 ".$date->format('H:i'));
    $done = [];
    $date->setTime($i,0);
    $c = clone $date;
    $c->setTime($i+1,0);
    foreach($reservations as $r){
        $d = new DateTime($r["date_reserv"]);
        $d = new DateTime("2021-01-10 ".$d->format('H:i'));
        $d2 = new DateTime($r["date_retour"]);
        $d2 = new DateTime("2021-01-10 ".$d2->format('H:i'));
        if( ( $d < $c && $d2 > $date)){
            array_push($done,$r);
        }
    }
    return $done;
}
function getThisDayRes($date,$all){
    $done = [];
    $date = new DateTime($date->format('Y-m-d'));
    foreach($all as $r){
        $d = new DateTime($r["date_reserv"]);
        $d = new DateTime($d->format('Y-m-d'));
        if( $date == $d){
            array_push($done,$r);
        }
    }
    return $done;
}
?>