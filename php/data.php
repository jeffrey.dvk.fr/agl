<?php
require_once '_dataFormatter.php';
require_once '_dbfunction.php';

//Les colonnes doivent être placés dans le même ordre que dans le tableau de la page
$colonne = [ "id","objet", "date_reserv",'date_retour'];
$table = "reservation";
// Pour la recherche et l'ordonnancement à ne pas modifier
$query = "SELECT * FROM  $table ";

if(!empty($_POST["search"]["value"]))
{	// changer les colonnes à rechercher
	$query .= "WHERE id LIKE '%".$_POST["search"]["value"]."%' ";
	$query .= "OR objet        LIKE '%".$_POST["search"]["value"]."%' ";
	$query .= "OR date_reserv          LIKE '%".$_POST["search"]["value"]."%' ";
	$query .= "OR date_retour          LIKE '%".$_POST["search"]["value"]."%' ";

}

// Filtrage dans le tableau
if(isset($_POST['order']))
	$query .= 'ORDER BY '.$colonne[$_POST['order']['0']['column']].' '.$_POST['order']['0']['dir'].' ';
else
	$query .= 'ORDER BY id DESC ';

if($_POST['length'] != -1)
	$query .= 'LIMIT ' . $_POST['start'] . ', ' . $_POST['length'];

$statement = $pdo->prepare($query);
$statement->execute();
$result = $statement->fetchAll();
$filtered_rows = $statement->rowCount();


// Pour le formattage de l'affichage des valeurs dans la table
$data = [];
foreach($result as $row){
    $data[] = [
                'id'            =>  $row['id'],
                'objet'         =>  $row['objet'],
                'date_reserv'   =>  formatDate($row['date_reserv']),
                'date_retour'   =>  formatDate($row['date_retour']),
                'status'        =>  state($row["status"]>0),
                'action'        =>  getActionsBtn( checkState($row), $row['id'] )    
            ];
}
$output = array(
	"draw"			=>	intval($_POST["draw"]),
	"recordsTotal"  	=>  $filtered_rows,
	"recordsFiltered" 	=> 	get_total_all_records($pdo),
	"data"				=>	$data
);
// Vos fonctions personnelles à partir d'ici 
echo json_encode($output);

function get_total_all_records($pdo){
    global $table;
	$statement = $pdo->prepare("SELECT * FROM $table"); // same query as above
	$statement->execute();
	return $statement->rowCount();
}
function checkState($row){
    $btns = [];
    if($row["type"]> 0){
        $btns += ['view'    => 'Voir dans le planning'];
    }
    if($row["status"]>0){
        $btns += ['cancel'    => 'Annuler'];
    }
    return $btns;
}