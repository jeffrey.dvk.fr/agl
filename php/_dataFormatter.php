<?php
function etat($column,$true){
    return $column == $true?
    '<center><span class="badge badge-primary">'. $column .'</span></center>' :
    '<center><span class="badge badge-warning">'. $column .'</span></center>' ;
}
function state($true){
    return $true ?
    '<center><span class="badge bg-success">Actif</span></center>' :
    '<center><span class="badge bg-secondary">Inactif</span></center>' ;
}
function getActionsBtn($buttons,$id){
    $output = '';
    if($buttons != []){
        $output = '<center><div class="btn-group">
    <button type="button" class="btn btn-primary dropdown-toggle" data-bs-toggle="dropdown" aria-expanded="false">
      Action
    </button>
    <ul class="dropdown-menu">';
    
    foreach($buttons as $key => $value){
        $output .= '<li><a class="dropdown-item '.$key.'" id="'.$id.'" href="#">'.$value.'</a></li>';
    }
    $output .= '</ul></div></center>';
    }
    return $output;
}
function formatDate($date,$type = 1){
    if(is_null($date)){
        return "-";
    }
    $zone = $type == 1 ? 'fr_FR' : 'en_US';
    $fmt = new IntlDateFormatter($zone,
    IntlDateFormatter::FULL,
    IntlDateFormatter::SHORT,
    'America/Los_Angeles',
    IntlDateFormatter::GREGORIAN
    );
    $date = strtotime($date);
    $date = $fmt->format($date);
    $date[0] = strtoupper($date[0]);
    return $date;
}